package ru.tsc.anaumova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.anaumova.tm.model.Project;
import ru.tsc.anaumova.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Show project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("[ENTER ID:]");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}